import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
    constructor(props){
        super();
        this.props = props;
    }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="App-intro">
            <div className="App-card">
                <h2 className="App-name">{this.props.name}</h2>
                <p className="App-year">{this.props.year}</p>
                <img src={this.props.src} alt=""/>
            </div>
        </div>
      </div>
    );
  }
}

export default App;
